﻿using System;

namespace Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.InputEncoding = System.Text.Encoding.UTF8;
            int n, pos=0,neg=0,odd=0,even=0;
            bool ch;
            Console.WriteLine("Вводьте цілі числа натискаючи Enter. Нуль завершує ввід.");
            do
            {
                do
                {
                    ch = int.TryParse(Console.ReadLine(), out n);
                    if (!ch) Console.WriteLine("Помилка введення, повторіть спробу.");
                } while (!ch);
                if (n == 0) break;
                if (n > 0)
                {
                    pos++;
                }
                else neg++;
                if (n % 2 == 0)
                {
                    even++;
                }
                else odd++;

            } while (n != 0);
            Console.WriteLine("Кількість додатніх чисел  -> {0}\nКількість від'ємних чисел -> {1}\n" +
                "Кількість парних чисел    -> {2}\nКількість непарних чисел  -> {3}", pos,neg, even, odd);
        }
    }
}