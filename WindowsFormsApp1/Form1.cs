﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Lab1 : Form
    {
        public Lab1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double x, y, z, res;
            bool check;
            check = double.TryParse(txtX.Text, out x);
            if (!check)
            {
                MessageBox.Show("Помилка, неправильно введене значення Х!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            check = double.TryParse(txtY.Text, out y);
            if (!check)
            {
                MessageBox.Show("Помилка, неправильно введене значення Y!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            check = double.TryParse(txtZ.Text, out z);
            if (!check)
            {
                MessageBox.Show("Помилка, неправильно введене значення Z!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            res = 5 * Math.Cos(x) - (z / 4) * Math.Sin(y) * ((4 * Math.Abs(z - x) - 5) / (z * (z - y) + Math.Pow(x, 2)));
            txtRes.Text = res.ToString("F2");
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
