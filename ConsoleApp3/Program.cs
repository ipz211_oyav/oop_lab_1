﻿using System;

namespace Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.InputEncoding = System.Text.Encoding.UTF8;

            int n, k, i; double sum=0;
            
            bool ch;
            Console.WriteLine("Введіть ціле додатнє значення");
            do
            {
                Console.Write("N = ");
                ch = int.TryParse(Console.ReadLine(), out n);
                if (!ch || n<=0) Console.WriteLine("Помилка введення, повторіть спробу.");
            } while (!ch || n<=0);
            for(i = 1, k = n; i<=n; i++, k-- )
            {
                sum +=Math.Pow(i, k); 
            }
            Console.WriteLine("{0}", sum);

        }
    }
}