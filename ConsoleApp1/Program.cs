﻿using System;

 namespace Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.InputEncoding = System.Text.Encoding.UTF8;

            double x, y, z, res;
            bool check;
            Console.WriteLine("Введіть дробові значення x, y, z");
            do
            {
                Console.Write("x = ");
                check = double.TryParse(Console.ReadLine(), out x);
                if (!check) Console.WriteLine("Помилка введення,  повторіть спробу");
            } while (!check);
            do
            {
                Console.Write("y = ");
                check = double.TryParse(Console.ReadLine(), out y);
                if (!check) Console.WriteLine("Помилка введення,  повторіть спробу");
            } while (!check);
            do
            {
                Console.Write("z = ");
                check = double.TryParse(Console.ReadLine(), out z);
                if (!check) Console.WriteLine("Помилка введення,  повторіть спробу");
            } while (!check);
            res = 5 * Math.Cos(x) - (z / 4) * Math.Sin(y) * ((4 * Math.Abs(z - x) - 5) / (z * (z - y) + Math.Pow(x, 2)));
            Console.WriteLine("Результат = {0:F2}", res);
        }
            
    }
}