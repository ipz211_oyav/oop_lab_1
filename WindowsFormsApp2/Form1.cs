﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class FormD : Form
    {
        public FormD()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                       System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void label2_Click(object sender, EventArgs e)
        {

        }
        private void label3_Click(object sender, EventArgs e)
        {

        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Boxx2.Visible = false;
            Boxx1.Visible = false;
            labelx1.Visible = false;
            labelx2.Visible = false;
            label4.Visible = false;
            label5.Visible = false;
            label6.Visible = false;

            double a, b, c, x1, x2;
            bool check;
            check = double.TryParse(textBoxA.Text, out a);
            if (!check)
            {
                MessageBox.Show("Помилка, неправильно введене значення a!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            check = double.TryParse(textBoxB.Text, out b);
            if (!check)
            {
                MessageBox.Show("Помилка, неправильно введене значення b!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            check = double.TryParse(textBoxC.Text, out c);
            if (!check)
            {
                MessageBox.Show("Помилка, неправильно введене значення c!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (a == 0 && b != 0 && c != 0)
            {
                x1 = (-c) / b;
                label4.Visible = true;
                label4.Text = String.Format("Рівняння: {0:F2}x + {1:F2} = 0", b, c);
                Boxx1.Visible = true;
                labelx1.Visible = true;
                Boxx1.Text = x1.ToString("F3");
            }
            else if (a == 0 && b != 0 && c == 0)
            {
                label4.Visible = true;
                label4.Text = String.Format("Рівняння: {0:F2}x = 0", b);
                Boxx1.Visible = true;
                labelx1.Visible = true;
                Boxx1.Text = ("0");
            }
            else if (a == 0 && b == 0 && c == 0)
            {
                label4.Visible = true;
                label4.Text = ("Усі змінні дорівнюють нулю. Розв'язків безліч.");
            }
            else if (a == 0 && b == 0 && c != 0)
            {
                label4.Visible = true;
                label4.Text = ("Розв'язків немає");
            }
            else if (a != 0 && b == 0 && c == 0)
            {
                labelx1.Visible = true;
                Boxx1.Visible = true;
                Boxx1.Text = String.Format("0");
            }
            else if (a != 0 && b != 0 && c == 0)
            {
                x1 = 0;
                x2 = b / a;
                label4.Visible = true;
                label4.Text = String.Format("Рівняння: {0:F2}x^2 + {1:F2}x = 0", a, b);
                labelx1.Visible=true;
                labelx2.Visible=true;
                Boxx1.Visible=true;
                Boxx2.Visible=true;
                Boxx1.Text = x1.ToString("F3");
                Boxx2.Text = x2.ToString("F3");
            }
            else if (a != 0 && b == 0 && c != 0)
            {
                label4.Visible= true;
                label4.Text = String.Format("Рівняння: {0:F2}x^2 + {1:F2} = 0\n", a, c);
                if ((c / a) > 0)
                {
                    label6.Visible=true;  
                    label6.Text = String.Format("Розв'язків немає!\nx^2 != {0:F3}", ((-c) / a));
                }
                else
                {
                    x1 = Math.Sqrt((-c) / a);
                    x2 = -Math.Sqrt((-c) / a);
                    labelx1.Visible = true;
                    labelx2.Visible = true;
                    Boxx1.Visible = true;
                    Boxx2.Visible = true;
                    Boxx1.Text = x1.ToString("F3");
                    Boxx2.Text = x2.ToString("F3");
                }
            }
            else if (a != 0 && b != 0 && c != 0)
            {
                label4.Visible= true;
                label4.Text = String.Format("Рівняння: {0:F2}x^2 + {1:F2}x + {2:F2} = 0", a, b, c);
                double d = b * b - 4 * a * c;
                label5.Visible = true;
                label5.Text  = String.Format("Дискримінант: {0:F2} - ({1:F2}) = {2:F2}", (b * b), (4 * a * c), d);
                if (d < 0)
                {
                    label6.Visible = true;
                    label6.Text = String.Format("Дискримінант менше нуля! Розв'язків немає.");
                }
                else if (d == 0)
                {
                    x1 = -b / (2 * a);
                    label6.Visible= true;
                    label6.Text = String.Format("Дискримінант дорівнює нулю.");
                    labelx1.Visible = true;
                    Boxx1.Visible = true;
                    Boxx1.Text = x1.ToString("F3");
                }
                else if (d > 0)
                {
                    x1 = (-b + d) / (2 * a);
                    x2 = (-b - d) / (2 * a);
                    labelx1.Visible = true;
                    Boxx1.Visible = true;
                    labelx2.Visible = true;
                    Boxx2.Visible = true;
                    Boxx1.Text = x1.ToString("F3");
                    Boxx2.Text = x2.ToString("F3");
                }
                 
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void labelx1_Click(object sender, EventArgs e)
        {

        }

        private void Boxx2_TextChanged(object sender, EventArgs e)
        {
         
        }
    }
}
