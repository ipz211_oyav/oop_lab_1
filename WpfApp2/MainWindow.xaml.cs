﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp2
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Boxx2.Visibility = Visibility.Collapsed;
            Boxx1.Visibility = Visibility.Collapsed;
            labelx1.Visibility = Visibility.Collapsed;
            labelx2.Visibility = Visibility.Collapsed;
            label4.Visibility = Visibility.Collapsed;
            label5.Visibility = Visibility.Collapsed;
            label6.Visibility = Visibility.Collapsed;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                       System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void TextBoxA_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Boxx2.Visibility = Visibility.Collapsed;
            Boxx1.Visibility = Visibility.Collapsed;
            labelx1.Visibility = Visibility.Collapsed;
            labelx2.Visibility = Visibility.Collapsed;
            label4.Visibility = Visibility.Collapsed;
            label5.Visibility = Visibility.Collapsed;
            label6.Visibility = Visibility.Collapsed;

            double a, b, c, x1, x2;
            bool check;
            check = double.TryParse(textBoxA.Text, out a);
            if (!check)
            {
                MessageBox.Show("Помилка, неправильно введене значення a!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            check = double.TryParse(textBoxB.Text, out b);
            if (!check)
            {
                MessageBox.Show("Помилка, неправильно введене значення b!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            check = double.TryParse(textBoxC.Text, out c);
            if (!check)
            {
                MessageBox.Show("Помилка, неправильно введене значення c!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (a == 0 && b != 0 && c != 0)
            {
                x1 = (-c) / b;
                label4.Visibility = Visibility.Visible;
                label4.Content = String.Format("Рівняння: {0:F2}x + {1:F2} = 0", b, c);
                Boxx1.Visibility = Visibility.Visible;
                labelx1.Visibility = Visibility.Visible;
                Boxx1.Text = x1.ToString("F3");
            }
            else if (a == 0 && b != 0 && c == 0)
            {
                label4.Visibility = Visibility.Visible;
                label4.Content = String.Format("Рівняння: {0:F2}x = 0", b);
                Boxx1.Visibility = Visibility.Visible;
                labelx1.Visibility = Visibility.Visible;
                Boxx1.Text = ("0");
            }
            else if (a == 0 && b == 0 && c == 0)
            {
                label4.Visibility = Visibility.Visible;
                label4.Content = ("Усі змінні дорівнюють нулю. Розв'язків безліч.");
            }
            else if (a == 0 && b == 0 && c != 0)
            {
                label4.Visibility = Visibility.Visible;
                label4.Content = ("Розв'язків немає");
            }
            else if (a != 0 && b == 0 && c == 0)
            {
                labelx1.Visibility = Visibility.Visible;
                Boxx1.Visibility = Visibility.Visible;
                Boxx1.Text = ("0");
            }
            else if (a != 0 && b != 0 && c == 0)
            {
                x1 = 0;
                x2 = b / a;
                label4.Visibility = Visibility.Visible;
                label4.Content = String.Format("Рівняння: {0:F2}x^2 + {1:F2}x = 0", a, b);
                labelx1.Visibility = Visibility.Visible;
                labelx2.Visibility = Visibility.Visible;
                Boxx1.Visibility = Visibility.Visible;
                Boxx2.Visibility = Visibility.Visible;
                Boxx1.Text = x1.ToString("F3");
                Boxx2.Text = x2.ToString("F3");
            }
            else if (a != 0 && b == 0 && c != 0)
            {
                label4.Visibility = Visibility.Visible;
                label4.Content = String.Format("Рівняння: {0:F2}x^2 + {1:F2} = 0\n", a, c);
                if ((c / a) > 0)
                {
                    label6.Visibility = Visibility.Visible;
                    label6.Content = String.Format("Розв'язків немає!\nx^2 != {0:F3}", ((-c) / a));
                }
                else
                {
                    x1 = Math.Sqrt((-c) / a);
                    x2 = -Math.Sqrt((-c) / a);
                    labelx1.Visibility = Visibility.Visible;
                    labelx2.Visibility = Visibility.Visible;
                    Boxx1.Visibility = Visibility.Visible;
                    Boxx2.Visibility = Visibility.Visible;
                    Boxx1.Text = x1.ToString("F3");
                    Boxx2.Text = x2.ToString("F3");
                }
            }
            else if (a != 0 && b != 0 && c != 0)
            {
                label4.Visibility = Visibility.Visible;
                label4.Content = String.Format("Рівняння: {0:F2}x^2 + {1:F2}x + {2:F2} = 0", a, b, c);
                double d = b * b - 4 * a * c;
                label5.Visibility = Visibility.Visible;
                label5.Content = String.Format("Дискримінант: {0:F2} - ({1:F2}) = {2:F2}", (b * b), (4 * a * c), d);
                if (d < 0)
                {
                    label6.Visibility = Visibility.Visible;
                    label6.Content = String.Format("Дискримінант менше нуля! Розв'язків немає.");
                }
                else if (d == 0)
                {
                    x1 = -b / (2 * a);
                    label6.Visibility = Visibility.Visible;
                    label6.Content = String.Format("Дискримінант дорівнює нулю.");
                    labelx1.Visibility = Visibility.Visible;
                    Boxx1.Visibility = Visibility.Visible;
                    Boxx1.Text = x1.ToString("F3");
                }
                else if (d > 0)
                {
                    x1 = (-b + d) / (2 * a);
                    x2 = (-b - d) / (2 * a);
                    labelx1.Visibility = Visibility.Visible;
                    Boxx1.Visibility = Visibility.Visible;
                    labelx2.Visibility = Visibility.Visible;
                    Boxx2.Visibility = Visibility.Visible;
                    Boxx1.Text = x1.ToString("F3");
                    Boxx2.Text = x2.ToString("F3");
                }
            }
        }
    }
}
