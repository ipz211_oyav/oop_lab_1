﻿using System;

namespace Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.InputEncoding = System.Text.Encoding.UTF8;

            double a, b, c, x1, x2;
            bool check;
            Console.WriteLine("Введіть значення a, b, c");
            do
            {
                Console.Write("a = ");
                check = double.TryParse(Console.ReadLine(), out a);
                if (!check) Console.WriteLine("Помилка введення,  повторіть спробу");
            } while (!check);
            do
            {
                Console.Write("b = ");
                check = double.TryParse(Console.ReadLine(), out b);
                if (!check) Console.WriteLine("Помилка введення,  повторіть спробу");
            } while (!check);
            do
            {
                Console.Write("c = ");
                check = double.TryParse(Console.ReadLine(), out c);
                if (!check) Console.WriteLine("Помилка введення,  повторіть спробу");
            } while (!check);
            if (a == 0 && b != 0 && c != 0)
            {
                x1 = (-c) / b;
                Console.WriteLine("Рівняння: {0:F2}x + {1:F2} = 0\nХ = {2:F3}",b,c, x1);
            }
            else if (a == 0 && b != 0 && c == 0)
            {
                Console.WriteLine("Рівняння: {0:F2}x = 0\nХ = 0", b);
            }
            else if (a == 0 && b == 0 && c == 0)
            {
                Console.WriteLine("Усі змінні дорівнюють нулю. Розв'язків безліч.");
            }
            else if (a != 0 && b != 0 && c == 0)
            {
                x1 = 0;
                x2 = b / a;
                Console.WriteLine("Рівняння: {0:F2}x^2 + {1:F2}x = 0\nХ1 = {2:F3}\n Х2 = {3:F3}", a, b, x1, x2);
            }
            else if(a != 0 && b == 0 && c==0)
            {
                Console.WriteLine("X = 0");
            }
            else if (a != 0 && b == 0 && c != 0)
            {
                Console.WriteLine("Рівняння: {0:F2}x^2 + {1:F2} = 0\n", a, c);
                if ((c / a) > 0)
                {
                    Console.WriteLine("Розв'язків немає.\nx^2 != {0:F3}", ((-c) / a));
                }
                else
                {
                    x1 = Math.Sqrt((-c) / a);
                    x2 = Math.Sqrt((-c) / a);
                    Console.WriteLine("X1 = {0:F3}\nX2 = {1:F3}", x1, (-x2));
                }
            }
            else if (a != 0 && b != 0 && c != 0)
            {
                Console.WriteLine("Рівняння: {0:F2}x^2 + {1:F2}x + {2:F2} = 0", a, b, c);
                double d = b*b - 4*a*c;
                Console.WriteLine("Дискримінант: {0:F2} - ({1:F2}) = {2:F2}", (b * b), (4 * a * c), d);
                if (d < 0)
                {
                    Console.WriteLine("Дискримінант менше нуля. Розв'язків немає.");
                }
                else if (d == 0)
                {
                    x1 = -b / (2 * a);
                    Console.WriteLine("Дискримінант дорівнює нулю.\nX = {0:F3}", x1);
                }
                else if (d > 0)
                {
                    x1 = (-b + d) / (2 * a);
                    x2 = (-b - d) / (2 * a);
                    Console.WriteLine("X1 = {0:F3}\nX2 = {1:F3}", x1, x2);
                }    

            }
          
        }
    }
}