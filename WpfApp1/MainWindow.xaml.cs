﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double x, y, z, res;
            bool check;
            check = double.TryParse(txtX.Text, out x);
            if (!check)
            {
                MessageBox.Show("Помилка, неправильно введене значення Х!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            check = double.TryParse(txtY.Text, out y);
            if (!check)
            {
                MessageBox.Show("Помилка, неправильно введене значення Y!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            check = double.TryParse(txtZ.Text, out z);
            if (!check)
            {
                MessageBox.Show("Помилка, неправильно введене значення Z!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            res = 5 * Math.Cos(x) - (z / 4) * Math.Sin(y) * ((4 * Math.Abs(z - x) - 5) / (z * (z - y) + Math.Pow(x, 2)));
            txtRes.Text = res.ToString("F2");
        }

    }
}
